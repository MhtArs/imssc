<?php
/*
 * Template Name: Front Page
 */
/**
 * imssc template for displaying the Front-Page
 *
 * @package WordPress
 * @subpackage imssc
 * @since imssc 1.0
 */

get_header(); ?>

	<div class="home-widgets"><?php
		if ( function_exists( 'dynamic_sidebar' ) ) :
			dynamic_sidebar( 'home-sidebar' );
		endif; ?>
	</div>

	<section class="page-content primary" role="main">
		<div id="home-slide" class="owl-carousel owl-theme">
 			<?php for($i = 1; $i <= 25; $i++) { ?>
 				<div class="item"><img class="lazyOwl" 
 				data-src="<?php echo get_template_directory_uri() . '/images/slide/slide_' . $i . '.jpg'; ?>" alt=""></div>
 			<?php } ?>
		</div>
		
		<?php
			if ( have_posts() ) : the_post();

				get_template_part( 'loop' ); ?>

				<aside class="post-aside"><?php

					wp_link_pages(
						array(
							'before'           => '<div class="linked-page-nav"><p>' . sprintf( __( '<em>%s</em> is separated in multiple parts:', 'imssc' ), get_the_title() ) . '<br />',
							'after'            => '</p></div>',
							'next_or_number'   => 'number',
							'separator'        => ' ',
							'pagelink'         => __( '&raquo; Part %', 'imssc' ),
						)
					); ?>

					<?php
						if ( comments_open() || get_comments_number() > 0 ) :
							comments_template( '', true );
						endif;
					?>

				</aside><?php

			else :

				get_template_part( 'loop', 'empty' );

			endif;
		?>
	</section>

<?php get_footer(); ?>