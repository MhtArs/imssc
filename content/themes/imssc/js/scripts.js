(function($) {
    
    if($("#home-slide").length) {
    	$("#home-slide").owlCarousel({
          navigation : false, // Show next and prev buttons
          pagination: true, // dots.
          slideSpeed : 500,
          paginationSpeed : 400,
          dotsEach: 3,
          
          singleItem:true,
          lazyLoad : true, // images load when they needed.
          
          autoPlay: 5000 //Set AutoPlay to 5 seconds
        });
    }
    
    $("a#hamburger").on("click", function(e) {
        $(".menu_mobile").toggleClass("open");
        e.preventDefault();
    });
})(jQuery);