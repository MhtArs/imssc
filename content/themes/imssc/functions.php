<?php
/**
 * imssc functions file
 *
 * @package WordPress
 * @subpackage imssc
 * @since imssc 1.0
 */


/******************************************************************************\
	Theme support, standard settings, menus and widgets
\******************************************************************************/

add_theme_support( 'post-formats', array( 'image', 'quote', 'status', 'link' ) );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );

$custom_header_args = array(
	'width'         => 980,
	'height'        => 200,
	'default-image' => get_template_directory_uri() . '/images/header.png',
);
add_theme_support( 'custom-header', $custom_header_args );

// register Foo_Widget widget
function register_imssc_news_widget() {
    register_widget( 'Imssc_News_Widget' );
}
add_action( 'widgets_init', 'register_imssc_news_widget' );

/**
 * Print custom header styles
 * @return void
 */
function imssc_custom_header() {
	$styles = '';
	if ( $color = get_header_textcolor() ) {
		echo '<style type="text/css"> ' .
				'.site-header .logo .blog-name, .site-header .logo .blog-description {' .
					'color: #' . $color . ';' .
				'}' .
			 '</style>';
	}
}
add_action( 'wp_head', 'imssc_custom_header', 11 );

$custom_bg_args = array(
	'default-color' => 'fba919',
	'default-image' => '',
);
add_theme_support( 'custom-background', $custom_bg_args );

register_nav_menu( 'main-menu', __( 'Your sites main menu', 'imssc' ) );

register_nav_menu( 'mobile-menu', __( 'Your sites mobile menu', 'imssc' ) );

if ( function_exists( 'register_sidebars' ) ) {
	register_sidebar(
		array(
			'id' => 'home-sidebar',
			'name' => __( 'Home widgets', 'imssc' ),
			'description' => __( 'Shows on home page', 'imssc' )
		)
	);

	register_sidebar(
		array(
			'id' => 'footer-sidebar',
			'name' => __( 'Footer widgets', 'imssc' ),
			'description' => __( 'Shows in the sites footer', 'imssc' )
		)
	);
}

if ( ! isset( $content_width ) ) $content_width = 650;

/**
 * Include editor stylesheets
 * @return void
 */
function imssc_editor_style() {
    add_editor_style( 'css/wp-editor-style.css' );
}
add_action( 'init', 'imssc_editor_style' );


/******************************************************************************\
	Scripts and Styles
\******************************************************************************/

/**
 * Enqueue imssc scripts
 * @return void
 */
function imssc_enqueue_scripts() {
	wp_enqueue_style( 'imssc-styles', get_stylesheet_uri(), array(), '1.02b' );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic|PT+Serif:400,700', array(), '1.0' );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3' );
	
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'default-scripts', get_template_directory_uri() . '/js/scripts.min.js', array(), '1.01f', true );
	
	if ( is_singular() ) 
		wp_enqueue_script( 'comment-reply' );
		
	if ( is_front_page() ) {
		// home slider.
		wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/css/owl.theme.css' );
		wp_enqueue_style( 'owl', get_template_directory_uri() . '/css/owl.carousel.css' );
		wp_enqueue_script( 'owl-slider', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	}
}
add_action( 'wp_enqueue_scripts', 'imssc_enqueue_scripts' );

function imssc_inline_js() {
	// Adaptive images.
	echo "<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>";
	
	// Google Analytics. ?>
	<script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-27774756-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
<?php 
}
add_action( 'wp_head', 'imssc_inline_js' );

/******************************************************************************\
	Content functions
\******************************************************************************/

/**
 * Displays meta information for a post
 * @return void
 */
function imssc_post_meta() {
	if ( get_post_type() == 'post' ) {
		echo sprintf(
			__( 'Posted %s in %s%s by %s. ', 'imssc' ),
			get_the_time( get_option( 'date_format' ) ),
			get_the_category_list( ', ' ),
			get_the_tag_list( __( ', <b>Tags</b>: ', 'imssc' ), ', ' ),
			get_the_author_link()
		);
	}
	edit_post_link( __( ' (edit)', 'imssc' ), '<span class="edit-link">', '</span>' );
}

/**
 * Adds Foo_Widget widget.
 */
class Imssc_News_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'imssc_news_widget', // Base ID
			__( 'IMSSC News', 'imssc' ), // Name
			array( 
				'description' => __( 'Shows latest news', 'imssc' ),
				'before_title' => "<h2>",
				'after_title'  => "</h2>"
			) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		echo "<ul>";
		$query_args = array(
			"post_type"   => "post",
			"post_status" => "publish"
		);
		$news = new WP_Query( $query_args );
		if( $news->have_posts() ) {
			while( $news->have_posts() ): $news->the_post();
				$cont = get_the_content();
				$cont = ( strlen( $cont ) > 65 ) ? substr( $cont, 0, 60 ) . '<a href="' . get_permalink() . '">[...]</a>' : $cont;
				echo "<li>" . $cont . "</li>";
			endwhile;
		} else {
			echo "<li>No news</li>";
		}
		echo "</ul>";
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget