# IMSSC #

This repo is source of [http://imsscistanbul.com](imsscistanbul.com).

### Building ###

* Clone the repo
* Make sure you have nodejs, npm and ruby (for Sass).
* Do your best in content folder and make sure you don't mess up with wordpress folder.
* cd into `content/themes/imssc/grunt` and type `grunt dist`